package mis.pruebas;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "¡Hola Mundo!" );
        if (args.length > 0) {
            System.out.println("Estos son mis parámetros:");
            for (String a : args) {
                System.out.println(String.format("    -  '%s'", a));
            }
        } else {
            System.out.println("No tengo parámetros.");
        }

        if (args.length > 0) {
            if(args[0].equalsIgnoreCase("--listar")) {
                for(int i = 1; i < 4; ++i) {
                    System.out.println(String.format(" - Número %d", i));
                }
            } else if (args[0].equalsIgnoreCase("--letras")) {
                System.out.println("Estas son algunas letras:");
                System.out.println("  - Letra A");
                System.out.println("  - Letra B");
                System.out.println("  - Letra C");
            } else if (args[0].equalsIgnoreCase("--frutas")) {
                System.out.println("Estas son algunas frutas:");
                System.out.println("  - Manzana");
                System.out.println("  - Banana");
                System.out.println("  - Naranja");
            }
            else if (args[0].equalsIgnoreCase("--animales")) {
                System.out.println("Estos son algunos animales:");
                System.out.println(" - Perro");
                System.out.println(" - Gato");
                System.out.println(" - Liebre");
            }
            else if (args[0].equalsIgnoreCase("--plantas")) {
                System.out.println("Estos son algunas plantas:");
                System.out.println(" - Pasto");
                System.out.println(" - Llantén");
                System.out.println(" - Albahaca");
            }
        }

        System.out.println( "¡Chau Mundo!" );

    }
}
